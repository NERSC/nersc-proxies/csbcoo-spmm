# CSB COO SPMM

Simulates sparse symmetric CSB COO format times dense block vector, a
key operation for the LOBPCG solver in the MFDn configuration
interaction code.

## random matrix

The python script in `input_generator` generates a random matrix which
captures some of the main features of the MFDn matrix. In particular
the dynamic size of the matrix tiles which leads to load balance
issues.

```console
$ ./main.py -h
usage: main.py [-h] [-n DIMENSION] [-k TILESIZE] [-z NNZ] [-r MAXTILERHO]
               outputfile

Create a sparse CSB COO matrix

positional arguments:
  outputfile            name of output file

optional arguments:
  -h, --help            show this help message and exit
  -n DIMENSION, --dimension DIMENSION
                        matrix dimension
  -k TILESIZE, --tilesize TILESIZE
                        tile size
  -z NNZ, --nnz NNZ     number of nonzeros
  -r MAXTILERHO, --maxtilerho MAXTILERHO
                        max density of tiles
```			

Note: see `input_generator/README.md` for details

## compiling

There is just a simple Makefile with one source file. An OpenMP compiler
is currently required.

```
make
```

## usage

```console
$ ./spmm.x -h
usage: ./spmm.x [-h] [-m BLOCKSIZE] <inputfile>

Benchmark SPMM performance

Required arguments:
  <inputfile>  matrix in CSBCOO binary format

Optional arguments:
  -h           show this message and exit
  -m BLOCKSIZE blocksize for vector (default: 1 (spmv))
```

### Example

Generate a matrix:

```
./input-generator/main.py -n 524288 -k 512 -z 268435456 -r 0.1 524288.512.268435456.bin
100%|█████████████████████████████████████████████████| 268435456/268435456 [01:06<00:00, 4020236.80it/s]
wrote nnz=268435456 elements in 20548 tiles to 524288.512.268435456.bin
```

```console
$ for i in {1..4}; do OMP_NUM_THREADS=$i ./openmp.spmm.x ./524288.512.268435456.bin; done

Timer, seconds, MFlops
Read Matrix, 2.45672, 0
SPMM_diag, 0.00212373, 423.126
SPMM, 0.695791, 1540.61
SPMM_T, 0.72015, 1488.5

Timer, seconds, MFlops
Read Matrix, 2.45016, 0
SPMM_diag, 0.0015707, 572.108
SPMM, 0.38042, 2817.79
SPMM_T, 0.367287, 2918.55

Timer, seconds, MFlops
Read Matrix, 2.4547, 0
SPMM_diag, 0.00136404, 658.782
SPMM, 0.25977, 4126.52
SPMM_T, 0.253972, 4220.72

Timer, seconds, MFlops
Read Matrix, 2.43966, 0
SPMM_diag, 0.0013268, 677.275
SPMM, 0.207948, 5154.86
SPMM_T, 0.203462, 5268.52
```

## Copyright Notice

CSBCOO SPMM: Compressed Sparse Block Coordinate format sparse 
matrix dense vector multiply benchmark (CSBCOO SPMM) Copyright 
(c) 2020, The Regents of the University of California, through Lawrence 
Berkeley National Laboratory (subject to receipt of any required approvals 
from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.