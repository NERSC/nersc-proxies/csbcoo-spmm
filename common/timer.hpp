#pragma once

#include <chrono>
#include <string>
#include <iostream>
#include <iomanip>
#include <unordered_map>
#include <vector>

typedef std::chrono::high_resolution_clock::time_point timepoint;

class Timer {
  std::unordered_map<std::string, timepoint> tstart, tstop;
  std::unordered_map<std::string, double> tflops;
  std::vector<std::string> names;
public:
  void start(const std::string& name) {
    tstart[name] = std::chrono::high_resolution_clock::now();
    names.push_back(name);
  }
  void stop(const std::string& name) {
    tstop[name] = std::chrono::high_resolution_clock::now();
  }
  
  void stop(const std::string& name, double flops) {
    tstop[name] = std::chrono::high_resolution_clock::now();
    tflops[name] = flops;
  }
  double seconds(const std::string& name) {
    auto duration = tstop[name] - tstart[name];
    return std::chrono::duration<double>(duration).count();
  }
  void report() {
    std::cout << "\nTimer, seconds, MFlops\n";		 
    for (auto const& name : names) {
      double t = seconds(name);
      double mflops = (tflops[name]/t) / 1.0e6;
      std::cout << name << ", " << t << ", " << mflops << "\n";
    }
  }
};
