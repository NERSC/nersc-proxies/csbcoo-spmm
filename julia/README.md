# Julia version

## Install

Tested with Julia 1.5

## Usage

```console
$ julia -O3 spmm.jl -m 8 ../input-generator/524288.512.268435456.bin
m = 8 mflops = 1558.581424
```

```
$ for m in 1 2 4 8; do julia spmm.jl -m $m ../input-generator/524288.512.268435456.bin; done
m = 1 mflops = 774.265991
m = 2 mflops = 1111.297021
m = 4 mflops = 1377.553843
m = 8 mflops = 1550.686917
```
