#include "hash.hpp" // hash specialization for tuple
#include "timer.hpp"
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

struct Tile {
  long p, q; // block coords
  std::vector<std::tuple<short, short, float>> elements;
  Tile() : p(0), q(0){};
  Tile(long p, long q) : p(p), q(q){};
  void insert(short i, short j, float value) {
    elements.push_back(std::make_tuple(i, j, value));
  }
  void insertv(std::vector<short> i, std::vector<short> j,
               std::vector<float> v) {
    for (int k = 0; k < i.size(); k++)
      elements.push_back(std::make_tuple(i[k], j[k], v[k]));
  }
  size_t nnz() const { return elements.size(); }
};

struct Matrix {
  long long n; // dimension
  int k;       // tile size
  std::unordered_map<std::tuple<int, int>, Tile> tiles;
  Matrix(long long n, int k) : n(n), k(k){};
  void insert(Tile tile) { tiles[std::make_tuple(tile.p, tile.q)] = tile; }
};

Matrix read_input(std::string inputfile) {
  std::ifstream ifs(inputfile, std::ios::in | std::ios::binary);

  long long n; // matrix dimension
  int k;       // tile size

  if (!ifs.is_open())
    std::exit(2);

  // read header
  ifs.read(reinterpret_cast<char *>(&n), sizeof(n));
  ifs.read(reinterpret_cast<char *>(&k), sizeof(k));

  Matrix matrix(n, k);
  do { // loop over tiles
    // header
    int p, q, tile_nnz;
    ifs.read(reinterpret_cast<char *>(&p), sizeof(p));
    ifs.read(reinterpret_cast<char *>(&q), sizeof(q));
    ifs.read(reinterpret_cast<char *>(&tile_nnz), sizeof(tile_nnz));

    // data
    Tile tile(p, q);
    std::vector<short> i(tile_nnz), j(tile_nnz);
    std::vector<float> v(tile_nnz);
    ifs.read(reinterpret_cast<char *>(&i[0]), tile_nnz * sizeof(i[0]));
    ifs.read(reinterpret_cast<char *>(&j[0]), tile_nnz * sizeof(j[0]));
    ifs.read(reinterpret_cast<char *>(&v[0]), tile_nnz * sizeof(v[0]));
    tile.insertv(i, j, v);

    matrix.insert(tile);
  } while (ifs.peek() != std::ifstream::traits_type::eof());

  return matrix;
}

void usage(char *argv[]) {
  std::cout << "usage: " << argv[0] << " [-h] [-m BLOCKSIZE] <inputfile>\n\n"
            << "Benchmark SPMM performance\n\n"
            << "Required arguments:\n"
            << "  <inputfile>  matrix in CSBCOO binary format\n\n"
            << "Optional arguments:\n"
            << "  -h           show this message and exit\n"
            << "  -m BLOCKSIZE blocksize for vector (default: 1 (spmv))\n"
            << std::endl;
}

int main(int argc, char *argv[]) {
  int m = 1;
  std::string inputfile;

  int iarg = 0;
  while ((iarg = getopt(argc, argv, "m:h")) != -1) {
    switch (iarg) {
    case 'm':
      m = std::atoi(optarg);
      break;
    case 'h':
      usage(argv);
      std::exit(1);
    }
  }

  if (optind == argc - 1) {
    inputfile = std::string(argv[optind]);
  } else {
    usage(argv);
    std::exit(1);
  }

  Timer t;
  t.start("Read Matrix");
  auto A = read_input(inputfile);
  t.stop("Read Matrix");

  std::vector<float> X(A.n * m, 1.0), Y(A.n * m, 2.0);

  long ntiles = A.n / A.k;
  long long flops = 0;
  long long flops_diagonal = 0;

  for (auto const &[coords, tile] : A.tiles) {
    const auto &[i, j] = coords;
    if (i == j) {
      flops_diagonal += 2 * m * tile.nnz();
    } else {
      flops += 4 * m * tile.nnz();
    }
  }

  t.start("SPMM_diag");

  for (long i = 0; i < ntiles; i++) {
    auto it = A.tiles.find(std::make_tuple(i, i));
    if (it != A.tiles.end()) {
      auto tile = it->second;
      for (auto const &[it, jt, value] : tile.elements) {
        auto r = i * A.k + it;
        auto c = i * A.k + jt;
        for (int b = 0; b < m; b++) {
          Y[r + b] += value * X[c + b];
        }
      }
    }
  }
  t.stop("SPMM_diag", flops_diagonal);

  t.start("SPMM");
  for (auto const &[coords, tile] : A.tiles) {
    const auto &[i, j] = coords;
    if (i == j)
      continue;
    for (auto const &[it, jt, value] : tile.elements) {
      auto r = i * A.k + it;
      auto c = j * A.k + jt;
      for (int b = 0; b < m; b++) {
        Y[r + b] += value * X[c + b];
        Y[c + b] += value * X[r + b];
      }
    }
  }
  t.stop("SPMM", flops);

  t.report();

  return 0;
}
