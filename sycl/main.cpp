#include "hash.hpp" // hash specialization for tuple
#include "timer.hpp"
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include <CL/sycl.hpp>

template <typename T>
using atomic_ref_t =
    sycl::intel::atomic_ref<T, sycl::intel::memory_order_relaxed,
                            sycl::intel::memory_scope_device,
                            sycl::access::address_space::global_space>;

// this exception handler with catch async exceptions
static auto exception_handler = [](cl::sycl::exception_list eList) {
  for (std::exception_ptr const &e : eList) {
    try {
      std::rethrow_exception(e);
    } catch (std::exception const &e) {
#if _DEBUG
      std::cout << "Failure" << std::endl;
#endif
      std::terminate();
    }
  }
};

struct Matrix {
  long long n; // dimension
  int k;       // tile size
  long long nnz = 0;
  long *A_ptr;
  long *A_nnz;
  std::vector<short> A_i, A_j;
  std::vector<float> A_value;
  Matrix(long long n, int k) : n(n), k(k){};
};

Matrix read_input(std::string inputfile) {
  std::ifstream ifs(inputfile, std::ios::in | std::ios::binary);

  long long n; // matrix dimension
  int k;       // tile size

  if (!ifs.is_open())
    std::exit(2);

  // read header
  ifs.read(reinterpret_cast<char *>(&n), sizeof(n));
  ifs.read(reinterpret_cast<char *>(&k), sizeof(k));

  Matrix matrix(n, k);

  long ntiles = n / k;

  matrix.A_ptr = new long[ntiles * ntiles];
  matrix.A_nnz = new long[ntiles * ntiles];
  for (long i = 0; i < ntiles * ntiles; i++) {
    matrix.A_nnz[i] = 0;
    matrix.A_ptr[i] = 0;
  }

  matrix.nnz = 0;

  do { // loop over tiles
    // header
    int p, q, tile_nnz;
    ifs.read(reinterpret_cast<char *>(&p), sizeof(p));
    ifs.read(reinterpret_cast<char *>(&q), sizeof(q));
    ifs.read(reinterpret_cast<char *>(&tile_nnz), sizeof(tile_nnz));

    matrix.A_nnz[p * ntiles + q] = tile_nnz;
    matrix.A_ptr[p * ntiles + q] = matrix.nnz;
    matrix.nnz += tile_nnz;

    // data
    std::vector<short> i(tile_nnz), j(tile_nnz);
    std::vector<float> v(tile_nnz);
    ifs.read(reinterpret_cast<char *>(&i[0]), tile_nnz * sizeof(i[0]));
    ifs.read(reinterpret_cast<char *>(&j[0]), tile_nnz * sizeof(j[0]));
    ifs.read(reinterpret_cast<char *>(&v[0]), tile_nnz * sizeof(v[0]));

    matrix.A_i.insert(matrix.A_i.end(), i.begin(), i.end());
    matrix.A_j.insert(matrix.A_j.end(), j.begin(), j.end());
    matrix.A_value.insert(matrix.A_value.end(), v.begin(), v.end());

  } while (ifs.peek() != std::ifstream::traits_type::eof());

  return matrix;
}

void usage(char *argv[]) {
  std::cout << "usage: " << argv[0] << " [-h] [-m BLOCKSIZE] <inputfile>\n\n"
            << "Benchmark SPMM performance\n\n"
            << "Required arguments:\n"
            << "  <inputfile>  matrix in CSBCOO binary format\n\n"
            << "Optional arguments:\n"
            << "  -h           show this message and exit\n"
            << "  -m BLOCKSIZE blocksize for vector (default: 1 (spmv))\n"
            << std::endl;
}

int main(int argc, char *argv[]) {

  int m = 1;
  std::string inputfile;

  int iarg = 0;
  while ((iarg = getopt(argc, argv, "m:h")) != -1) {
    switch (iarg) {
    case 'm':
      m = std::atoi(optarg);
      break;
    case 'h':
      usage(argv);
      std::exit(1);
    }
  }

  if (optind == argc - 1) {
    inputfile = std::string(argv[optind]);
  } else {
    usage(argv);
    std::exit(1);
  }

  Timer t;
  t.start("Read Matrix");
  auto hA = read_input(inputfile);
  t.stop("Read Matrix");

  int tile_size = hA.k;
  short *hA_i = hA.A_i.data();
  short *hA_j = hA.A_j.data();
  float *hA_value = hA.A_value.data();
  long *hA_nnz = hA.A_nnz;
  long *hA_ptr = hA.A_ptr;

  float *hX = (float *)malloc(hA.n * m * sizeof(float));
  float *hY = (float *)malloc(hA.n * m * sizeof(float));

  for (long i = 0; i < hA.n * m; i++) {
    hX[i] = 1.0;
    hY[i] = 2.0;
  }

  long ntiles = hA.n / hA.k;
  long long flops = 0;
  long long flops_diagonal = 0;
  for (long i = 0; i < ntiles; i++) {
    flops_diagonal += 2 * m * hA_nnz[i * ntiles + i];
    for (long j = 0; j < i; j++) {
      flops += 4 * m * hA_nnz[i * ntiles + j];
    }
  }

  try {
    sycl::queue q(sycl::default_selector{}, exception_handler);

    sycl::range<1> dims{static_cast<size_t>(hA.n) * static_cast<size_t>(m)};
    sycl::range<1> sdim{static_cast<size_t>(hA.nnz)};
    sycl::range<1> tdim{static_cast<size_t>(ntiles)};
    sycl::range<2> tdims{static_cast<size_t>(ntiles),
                         static_cast<size_t>(ntiles)};
    sycl::buffer<float, 1> dX{hX, dims};
    sycl::buffer<float, 1> dY{hY, dims};
    sycl::buffer<short, 1> dAi{hA_i, sdim};
    sycl::buffer<short, 1> dAj{hA_j, sdim};
    sycl::buffer<float, 1> dAv{hA_value, sdim};
    sycl::buffer<long, 2> dAnnz{hA_nnz, tdims};
    sycl::buffer<long, 2> dAptr{hA_ptr, tdims};

    t.start("SPMM_diag");

    q.submit([&](sycl::handler &h) {
      auto A_nnz = dAnnz.get_access<sycl::access::mode::read>(h);
      auto A_ptr = dAptr.get_access<sycl::access::mode::read>(h);
      auto X = dX.get_access<sycl::access::mode::read>(h);
      auto Y = dY.get_access<sycl::access::mode::read_write>(h);
      auto Ai = dAi.get_access<sycl::access::mode::read>(h);
      auto Aj = dAj.get_access<sycl::access::mode::read>(h);
      auto Av = dAv.get_access<sycl::access::mode::read>(h);

      h.parallel_for<class spam_diag>(tdim, [=](sycl::id<1> ii) {
        auto nnz_tile = A_nnz[ii][ii];
        if (nnz_tile > 0) {
          auto tile_loc = A_ptr[ii][ii];
          for (int k = 0; k < nnz_tile; k++) {
            auto r = ii * tile_size + Ai[tile_loc + k];
            auto c = ii * tile_size + Aj[tile_loc + k];
            auto value = Av[tile_loc + k];
            for (int b = 0; b < m; b++) {
              atomic_ref_t<float> Yrb{Y[r + b]};
              Yrb += value * X[c + b];
            }
          }
        }
      });
    });

    q.wait();
    t.stop("SPMM_diag", flops_diagonal);

    t.start("SPMM");
    q.submit([&](sycl::handler &h) {
      auto A_nnz = dAnnz.get_access<sycl::access::mode::read>(h);
      auto A_ptr = dAptr.get_access<sycl::access::mode::read>(h);
      auto X = dX.get_access<sycl::access::mode::read>(h);
      auto Y = dY.get_access<sycl::access::mode::read_write>(h);
      auto Ai = dAi.get_access<sycl::access::mode::read>(h);
      auto Aj = dAj.get_access<sycl::access::mode::read>(h);
      auto Av = dAv.get_access<sycl::access::mode::read>(h);

      h.parallel_for<class spam>(tdims, [=](sycl::id<2> index) {
        auto ii = index[0];
        auto jj = index[1];
        if (ii != jj) {
          auto nnz_tile = A_nnz[ii][jj];
          if (nnz_tile > 0) {
            auto tile_loc = A_ptr[ii][jj];
            for (int k = 0; k < nnz_tile; k++) {
              auto r = ii * tile_size + Ai[tile_loc + k];
              auto c = jj * tile_size + Aj[tile_loc + k];
              auto value = Av[tile_loc + k];
              for (int b = 0; b < m; b++) {
                atomic_ref_t<float> Yrb{Y[r + b]};
                atomic_ref_t<float> Ycb{Y[c + b]};
                Yrb += value * X[c + b];
                Ycb += value * X[r + b];
              }
            }
          }
        }
      });
    });

    q.wait();
    t.stop("SPMM", flops);

  } catch (sycl::exception const &e) {
    std::cout << "An exception is caught while multiplying matrices.\n";
    std::terminate();
  }

  t.report();
  return 0;
}
